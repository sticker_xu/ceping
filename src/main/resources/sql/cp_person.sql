/*
 Navicat Premium Data Transfer

 Source Server         : ceping
 Source Server Type    : MySQL
 Source Server Version : 50719
 Source Host           : 123.57.21.174:3306
 Source Schema         : yhdb

 Target Server Type    : MySQL
 Target Server Version : 50719
 File Encoding         : 65001

 Date: 15/12/2020 10:30:37
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for cp_person
-- ----------------------------
DROP TABLE IF EXISTS `cp_person`;
CREATE TABLE `cp_person`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `name` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '姓名',
  `department` varchar(20) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '部门',
  `rank_gt` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '上级',
  `rank_eq` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '平级',
  `rank_lt` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '下级',
  `mark` tinyint(1) NULL DEFAULT 0 COMMENT '是否参与：0.未参与; 1.已参与',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 24 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of cp_person
-- ----------------------------
INSERT INTO `cp_person` VALUES (1, '向晖', '总经理', '董事长', '', '耿斌;唐泽远;周磊;刘淑新;董同新;孙婵娟;于雷;种伟;宋可鑫;王洋;宋庆;张洪波;邢辉;张思捷;翟利明;任云杰;王云军;姜文朋;郭英;孟建军;李新蕾;张蕾', 0);
INSERT INTO `cp_person` VALUES (2, '耿斌', '销售部', '向晖', '唐泽远;周磊;刘淑新;董同新;孙婵娟;于雷;种伟', '孟建军', 0);
INSERT INTO `cp_person` VALUES (3, '唐泽远', '策划部', '向晖', '耿斌;周磊;刘淑新;董同新;孙婵娟;于雷;种伟', '', 0);
INSERT INTO `cp_person` VALUES (4, '周磊', '产品总监', '向晖', '耿斌;唐泽远;刘淑新;董同新;孙婵娟;于雷;种伟', '宋庆;宋可鑫;王洋', 0);
INSERT INTO `cp_person` VALUES (5, '刘淑新', '质量总监', '向晖', '耿斌;唐泽远;周磊;董同新;孙婵娟;于雷;种伟', '张思捷;翟利明;任云杰;王云军;姜文朋', 0);
INSERT INTO `cp_person` VALUES (6, '董同新', '人力行政', '向晖', '耿斌;唐泽远;周磊;刘淑新;孙婵娟;于雷;种伟', '', 0);
INSERT INTO `cp_person` VALUES (7, '孙婵娟', '董秘', '向晖', '耿斌;唐泽远;周磊;刘淑新;董同新;于雷;种伟', '', 0);
INSERT INTO `cp_person` VALUES (8, '于雷', '财务总监', '向晖', '耿斌;唐泽远;周磊;刘淑新;董同新;孙婵娟;种伟', '郭英', 0);
INSERT INTO `cp_person` VALUES (9, '种伟', '人资总监', '向晖', '耿斌;唐泽远;周磊;刘淑新;董同新;孙婵娟;于雷', '李新蕾;张蕾', 0);
INSERT INTO `cp_person` VALUES (10, '宋可鑫', '研发部', '向晖;周磊', '王洋;宋庆;张洪波;邢辉;张思捷;翟利明;任云杰;王云军;姜文朋;郭英;孟建军;李新蕾;张蕾', '', 0);
INSERT INTO `cp_person` VALUES (11, '王洋', '产品部', '向晖;周磊', '宋可鑫;宋庆;张洪波;邢辉;张思捷;翟利明;任云杰;王云军;姜文朋;郭英;孟建军;李新蕾;张蕾', '', 0);
INSERT INTO `cp_person` VALUES (12, '宋庆', '产品部', '向晖;周磊', '宋可鑫;王洋;张洪波;邢辉;张思捷;翟利明;任云杰;王云军;姜文朋;郭英;孟建军;李新蕾;张蕾', '', 0);
INSERT INTO `cp_person` VALUES (13, '张洪波', '信息部', '向晖', '宋可鑫;王洋;宋庆;邢辉;张思捷;翟利明;任云杰;王云军;姜文朋;郭英;孟建军;李新蕾;张蕾', '', 0);
INSERT INTO `cp_person` VALUES (14, '邢辉', '信息部', '向晖', '宋可鑫;王洋;宋庆;张洪波;张思捷;翟利明;任云杰;王云军;姜文朋;郭英;孟建军;李新蕾;张蕾', '', 0);
INSERT INTO `cp_person` VALUES (15, '张思捷', '质量部', '向晖;刘淑新', '宋可鑫;王洋;宋庆;张洪波;邢辉;翟利明;任云杰;王云军;姜文朋;郭英;孟建军;李新蕾;张蕾', '', 0);
INSERT INTO `cp_person` VALUES (16, '翟利明', '物流计划部', '向晖;刘淑新', '宋可鑫;王洋;宋庆;张洪波;邢辉;张思捷;任云杰;王云军;姜文朋;郭英;孟建军;李新蕾;张蕾', '', 0);
INSERT INTO `cp_person` VALUES (17, '任云杰', '采购部', '向晖;刘淑新', '宋可鑫;王洋;宋庆;张洪波;邢辉;张思捷;翟利明;王云军;姜文朋;郭英;孟建军;李新蕾;张蕾', '', 0);
INSERT INTO `cp_person` VALUES (18, '王云军', '工艺部', '向晖;刘淑新', '宋可鑫;王洋;宋庆;张洪波;邢辉;张思捷;翟利明;任云杰;姜文朋;郭英;孟建军;李新蕾;张蕾', '', 0);
INSERT INTO `cp_person` VALUES (19, '姜文朋', '生产部', '向晖;刘淑新', '宋可鑫;王洋;宋庆;张洪波;邢辉;张思捷;翟利明;任云杰;王云军;郭英;孟建军;李新蕾;张蕾', '', 0);
INSERT INTO `cp_person` VALUES (20, '郭英', '财务部', '向晖;于雷', '宋可鑫;王洋;宋庆;张洪波;邢辉;张思捷;翟利明;任云杰;王云军;姜文朋;孟建军;李新蕾;张蕾', '', 0);
INSERT INTO `cp_person` VALUES (21, '孟建军', '售后实施', '向晖;耿斌', '宋可鑫;王洋;宋庆;张洪波;邢辉;张思捷;翟利明;任云杰;王云军;姜文朋;郭英;李新蕾;张蕾', '', 0);
INSERT INTO `cp_person` VALUES (22, '李新蕾', '人力行政', '向晖;种伟', '宋可鑫;王洋;宋庆;张洪波;邢辉;张思捷;翟利明;任云杰;王云军;姜文朋;郭英;孟建军;张蕾', '张蕾', 0);
INSERT INTO `cp_person` VALUES (23, '张蕾', '人力行政', '向晖;种伟;李新蕾', '宋可鑫;王洋;宋庆;张洪波;邢辉;张思捷;翟利明;任云杰;王云军;姜文朋;郭英;孟建军;李新蕾', '', 0);

SET FOREIGN_KEY_CHECKS = 1;
