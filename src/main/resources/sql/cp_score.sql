/*
 Navicat Premium Data Transfer

 Source Server         : ceping
 Source Server Type    : MySQL
 Source Server Version : 50719
 Source Host           : 123.57.21.174:3306
 Source Schema         : yhdb

 Target Server Type    : MySQL
 Target Server Version : 50719
 File Encoding         : 65001

 Date: 15/12/2020 10:30:45
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for cp_score
-- ----------------------------
DROP TABLE IF EXISTS `cp_score`;
CREATE TABLE `cp_score`  (
  `id` int(11) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `dimen1` tinyint(1) NULL DEFAULT 0 COMMENT '人才成长',
  `dimen2` tinyint(1) NULL DEFAULT 0 COMMENT '计划与执行',
  `dimen3` tinyint(1) NULL DEFAULT 0 COMMENT '团队管理与协调能力',
  `dimen4` tinyint(1) NULL DEFAULT 0 COMMENT '判断与决策能力',
  `dimen5` tinyint(1) NULL DEFAULT 0 COMMENT '专业知识与技能',
  `dimen6` tinyint(1) NULL DEFAULT 0 COMMENT '责任心与担当能力',
  `dimen7` tinyint(1) NULL DEFAULT 0 COMMENT '结果与客户导向',
  `rate` tinyint(11) NULL DEFAULT 0 COMMENT '打分人',
  `berate` tinyint(11) NULL DEFAULT 0 COMMENT '被打分人',
  `comment` varchar(300) CHARACTER SET utf8 COLLATE utf8_general_ci NULL DEFAULT '' COMMENT '点评建议',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 1 CHARACTER SET = utf8 COLLATE = utf8_general_ci ROW_FORMAT = Dynamic;

SET FOREIGN_KEY_CHECKS = 1;
