package com.yarward.ceping.mapper;

import com.yarward.ceping.entity.CPPerson;
import org.springframework.stereotype.Repository;

@Repository
public interface CPPersonMapper {
    CPPerson getById(Integer id);
    CPPerson getIdByName(String name);
    void updateById(Integer id);
}
