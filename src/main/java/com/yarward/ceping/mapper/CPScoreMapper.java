package com.yarward.ceping.mapper;

import com.yarward.ceping.entity.CPScore;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CPScoreMapper {
    void saveAll(List<CPScore> cpScores);
}
