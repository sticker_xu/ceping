package com.yarward.ceping.service.impl;

import com.yarward.ceping.entity.CPPerson;
import com.yarward.ceping.mapper.CPPersonMapper;
import com.yarward.ceping.service.CPPersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CPPersonServiceImpl implements CPPersonService {
    @Autowired
    private CPPersonMapper cpPersonMapper;
    @Override
    public CPPerson getById(Integer id) {
        return cpPersonMapper.getById(id);
    }

    @Override
    public CPPerson getIdByName(String name) {
        return cpPersonMapper.getIdByName(name);
    }

    @Override
    public void updateById(Integer id) {
        cpPersonMapper.updateById(id);
    }
}
