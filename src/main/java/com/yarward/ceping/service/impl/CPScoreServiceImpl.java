package com.yarward.ceping.service.impl;

import com.yarward.ceping.entity.CPScore;
import com.yarward.ceping.mapper.CPScoreMapper;
import com.yarward.ceping.service.CPScoreService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CPScoreServiceImpl implements CPScoreService {
    @Autowired
    private CPScoreMapper cpScoreMapper;
    @Override
    public void saveAll(List<CPScore> cpScores) {
        cpScoreMapper.saveAll(cpScores);
    }
}
