package com.yarward.ceping.service;


import com.yarward.ceping.entity.CPPerson;

public interface CPPersonService {
    CPPerson getById(Integer id);
    CPPerson getIdByName(String name);
    void updateById(Integer id);
}
