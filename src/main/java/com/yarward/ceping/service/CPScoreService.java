package com.yarward.ceping.service;


import com.yarward.ceping.entity.CPScore;

import java.util.List;

public interface CPScoreService {
    void saveAll(List<CPScore> cpScores);
}
