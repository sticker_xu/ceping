package com.yarward.ceping;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.servlet.error.ErrorController;
import org.springframework.context.annotation.Configuration;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

@SpringBootApplication
@MapperScan(basePackages = {"com.yarward.ceping.mapper"})
public class CepingApplication {

    public static void main(String[] args) {
        SpringApplication.run(CepingApplication.class, args);
    }

    @Controller
    class ErrController implements ErrorController {
        @Override
        public String getErrorPath() {
            return "error";
        }
        @RequestMapping("/error")
        public String error() {
            return getErrorPath();
        }
    }
    @Configuration
    class YhWebMvcConfig implements WebMvcConfigurer {
        @Override
        public void addResourceHandlers(ResourceHandlerRegistry registry) {
//            registry.addResourceHandler("/disk/**").addResourceLocations("file:E:/Application/data/");
            registry.addResourceHandler("/favicon.ico")//favicon.ico
                    .addResourceLocations("classpath:/static/");
        }
    }
}
