package com.yarward.ceping.util;

import org.apache.commons.lang3.StringUtils;

import java.math.BigDecimal;

public class ParameterUtil {
    public static Long toLong(Object param, Long defaultValue) {
        if (param instanceof BigDecimal) {
            return ((BigDecimal) param).longValue();
        } else if (param instanceof String) {
            return Long.valueOf(toString(param, null));
        } else {
            return defaultValue;
        }
    }
    public static boolean isBlank(String param) {
        return StringUtils.isBlank(param);
    }
    public static boolean isNotBlank(String param) {
        return !isBlank(param);
    }
    public static Integer toInteger(String param) {
        return toInteger(param, 0);
    }
    public static Integer toInteger(String param, Integer defaultValue) {
        return isBlank(param) ? defaultValue : Integer.valueOf(param);
    }
    public static BigDecimal toBigDecimal(String param, BigDecimal defaultValue) {
        return isBlank(param) ? defaultValue : new BigDecimal(param);
    }
    public static Double toDouble(Object param, Double defaultValue) {
        if (param instanceof BigDecimal) {
            return ((BigDecimal) param).doubleValue();
        } else if (param instanceof String) {
            return Double.valueOf(toString(param, null));
        } else {
            return defaultValue;
        }
    }
    public static Float toFloat(String param, Float defaultValue) {
        return isBlank(param) ? defaultValue : Float.valueOf(param);
    }
    public static String toString(Object param) {
        return toString(param, "");
    }
    public static String toString(Object param, String defaultValue) {
        return param == null ? defaultValue : param.toString();
    }
}
