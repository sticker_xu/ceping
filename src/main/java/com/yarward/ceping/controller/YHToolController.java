package com.yarward.ceping.controller;

import com.yarward.ceping.entity.CPPerson;
import com.yarward.ceping.entity.CPScore;
import com.yarward.ceping.service.CPPersonService;
import com.yarward.ceping.service.CPScoreService;
import com.yarward.ceping.util.ParameterUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/tool")
@Slf4j
public class YHToolController {
    @Autowired
    private CPPersonService cpPersonService;
    @Autowired
    private CPScoreService cpScoreService;
    @GetMapping({"","/"})
    public String index() {
        return "index";
    }

    @GetMapping("/ceping/{id:\\d+}")
    public String ceping(@PathVariable("id") Integer id, ModelMap modelMap) {
        String url = "error";
        String message = "";
        CPPerson person = cpPersonService.getById(id);
        if (person != null && person.getMark() == 0) {
            url = "tool/ceping";
            modelMap.put("person", person);
        } else if (person != null && person.getMark() == 1){
            message = "您已参与过本次测评，请勿重复参与，谢谢！";
        } else {
            message = "链接不存在！";
        }
        modelMap.put("message", message);
        return url;
    }

    @PostMapping("/ceping/sub")
    public void cepingsub(HttpServletRequest request, HttpServletResponse response) {
        response.setContentType("text/html;charset=UTF-8");
        Integer rate = ParameterUtil.toInteger(request.getParameter("rate"));
        CPPerson byId = cpPersonService.getById(rate);
        if (byId != null && byId.getMark() == 1) {
            try {
                response.getWriter().write("<script>alert('请勿重复提交！');history.back(-1);</script>");
                return;
            } catch (IOException e) {
                log.error(e.getMessage(), e);
            }
        }
        String[] dimen1s = request.getParameterValues("dimen1");
        String[] dimen2s = request.getParameterValues("dimen2");
        String[] dimen3s = request.getParameterValues("dimen3");
        String[] dimen4s = request.getParameterValues("dimen4");
        String[] dimen5s = request.getParameterValues("dimen5");
        String[] dimen6s = request.getParameterValues("dimen6");
        String[] dimen7s = request.getParameterValues("dimen7");
        String[] comments = request.getParameterValues("comment");
        String[] berates = request.getParameterValues("berate");
        List<CPScore> list = new ArrayList<>();
        CPScore score;
        CPPerson person;
        int size = berates.length;
        for (int i = 0; i < size; i++) {
            person = cpPersonService.getIdByName(berates[i]);
            score = new CPScore();
            score.setRate(rate);
            score.setBerate(person.getId());
            score.setDimen1(ParameterUtil.toInteger(dimen1s[i]));
            score.setDimen2(ParameterUtil.toInteger(dimen2s[i]));
            score.setDimen3(ParameterUtil.toInteger(dimen3s[i]));
            score.setDimen4(ParameterUtil.toInteger(dimen4s[i]));
            score.setDimen5(ParameterUtil.toInteger(dimen5s[i]));
            score.setDimen6(ParameterUtil.toInteger(dimen6s[i]));
            score.setDimen7(ParameterUtil.toInteger(dimen7s[i]));
            score.setComment(comments[i]);
            list.add(score);
        }
        cpScoreService.saveAll(list);
        log.info("评分表数据添加成功，共添加 {} 条数据", size);
        //修改点评人 参与标识
        cpPersonService.updateById(rate);
        log.info("修改点评人参与标识成功，点评人：{}", rate);
        try {
            response.getWriter().write("<script>alert('感谢您的参与！');history.back(-1);</script>");
        } catch (IOException e) {
            log.error(e.getMessage(), e);
        }
    }
}
