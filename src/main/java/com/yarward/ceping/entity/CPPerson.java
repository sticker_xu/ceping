package com.yarward.ceping.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class CPPerson implements Serializable {
    private Integer id;
    private String name;
    private String department;
    private String rank_gt;
    private String rank_eq;
    private String rank_lt;
    private Integer mark;
}
