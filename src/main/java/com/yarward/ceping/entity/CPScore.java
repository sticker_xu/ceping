package com.yarward.ceping.entity;

import lombok.Data;

import java.io.Serializable;

@Data
public class CPScore implements Serializable {
    private Integer id;
    private Integer dimen1;
    private Integer dimen2;
    private Integer dimen3;
    private Integer dimen4;
    private Integer dimen5;
    private Integer dimen6;
    private Integer dimen7;
    private Integer rate;
    private Integer berate;
    private String comment;
}
